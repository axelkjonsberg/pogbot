const Discord = require("discord.js");

const client = new Discord.Client();
client.login(process.env.BOT_TOKEN);

const prefix = "!"

client.on("message", message => {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length);
    // Bot currently does not use any arguments for its commands
    const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();

    if (command === "pog") {
        const ranInt = Math.floor(Math.random() * 6); 
        message.channel.send("", {files: [`./image-${ranInt}.png`]});
    }

    if (command === "frog") {
        message.channel.send("", {files: [`./image-5.png`]});
    }
});
